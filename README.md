# ipa_client role

Make a host member of an IPA / CentOS identity management domain


## Variables

* `ipa_admin_user`

    The IPA account used to add the device to the domain.

    This value must be provided, no defaults.

* `ipa_admin_passwd`

    Password of the IPA account used to add the device to the domain.

    This value must be provided, no defaults.

* `ipa_krb_realm`

    The IPA domain realm. This is usually the domain written in CAPS.

    This value must be provided, no defaults.

* `ipa_server`

    The IPA server to use for the domain registration.

* `ipa_mac_addresses`

    List of MAC addresses of the network interfaces facing the domain
    network on the client being added. (Can be multiple,
    e.g. ethernet + wifi.)

    Note: If an empty list is passed, any existing mac address will be
    unregistered.

    Default: undefined

* `ipa_dyndns`

    Whether Free IPA should update the DNS with the current IP
    address. Boolean.

    Default: true

* `ipa_dyndns_if`

    The interface to watch for IP address changes on to report back to
    DNS. If not specified, FreeIPA will self try to guess the IF.

    Default: _Undefined_

### Global variables

This is information that is not specific to IPA but rather general
information about the host or the network. This information's natural
residence is in the inventory but could be provided through other
means.

Information about these variables is found in the root level README.md
file.

* `network_domain`

* `network_ntp_servers`

* `host_description`

* `host_location`

* `host_owners`

* `host_interfaces`
